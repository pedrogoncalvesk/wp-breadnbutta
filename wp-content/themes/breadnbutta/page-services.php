<?php
/*
Template Name: Services Page
*/
?>

<?php get_header(); ?>

<div class="content-top">
	    <img src="<?php bloginfo('template_url'); ?>/images/turntable.gif" alt="DJ"/>
</div><!--/content-->

		
<?php include ('sidebar2.php'); ?>

<div class="wide-wrapper services-page">
		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
				
				<div class="post" id="post-<?php the_ID(); ?>">
				
					<h2 class="title"><span><?php the_title(); ?></span></h2>
					
					<div class="entry">						
												
						<?php the_content(); ?>
				
					</div>
					
					<?php edit_post_link('Edit', '<p>', '</p>'); ?>
					
				</div><!--/post-->
				
			<?php endwhile; ?>
		<?php else : ?>
		
			<div class="post">
				<h2><?php _e('Not Found'); ?></h2>
			</div>
			
		<?php endif; ?>
</div><!--/wrapper-->

<?php get_footer(); ?>