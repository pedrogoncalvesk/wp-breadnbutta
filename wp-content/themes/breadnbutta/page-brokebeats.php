<?php
/*
Template Name: Brokebeats Page
*/
?>

<?php get_header(); ?>

<div class="content-top">
	    <img src="<?php bloginfo('template_url'); ?>/images/turntable.gif" alt="DJ"/>
</div><!--/content-->

		
<?php include ('sidebar2.php'); ?>

<div class="wide-wrapper brokebeats-page">
		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
								
			<h2 class="title"><span><?php the_title(); ?></span></h2>
			
			<div class="clearfix clearBoth">
			<?php the_content(); ?>
			<?php edit_post_link('Edit', '<p>', '</p>'); ?>
			</div>
			
				<div class="content-left">
					<?php
				    query_posts('pagename=brokebeats/_left');
				    ?>
				    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				    <?php the_content(); ?>
				    <?php edit_post_link('Edit', '<p>', '</p>'); ?>
				    <?php endwhile; endif; ?>
				    <?php 
					wp_reset_query();
					?>
				</div><!--/content-left-->
					
				<div class="content-right">
					<?php
				    query_posts('pagename=brokebeats/_right');
				    ?>
				    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				    <?php the_content(); ?>
				    <?php edit_post_link('Edit', '<p>', '</p>'); ?>
				    <?php endwhile; endif; ?>
				    <?php 
					wp_reset_query();
					?>
				</div><!--/content-right-->
					
					
				
			<?php endwhile; ?>
		<?php else : ?>
		
			<div class="post">
				<h2><?php _e('Not Found'); ?></h2>
			</div>
			
		<?php endif; ?>
</div><!--/wrapper-->

<?php get_footer(); ?>