<?php get_header(); ?>

<div id="content">
	    <h1 class="title"><span>News</span></h1>

		<div class="wrapper">
		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
				<div class="post" id="post-<?php the_ID(); ?>">
				
					<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_time('F j, Y'); ?></a></h4>
					
					<div class="entry">						
												
						<?php the_content(); ?>
				
					</div>
					
				</div><!--/post-->
				
				<?php comments_template(); ?>
				
			<?php endwhile; ?>
		<?php else : ?>
		
			<div class="post">
				<h2><?php _e('Not Found'); ?></h2>
			</div>
			
		<?php endif; ?>
		</div><!--/wrapper-->
		
</div><!--/content-->
	
<?php get_sidebar(); ?>

<?php get_footer(); ?>