<?php
/*
Template Name: Artists Main Page
*/
?>

<?php get_header(); ?>

<div class="content-top">
	    <img src="<?php bloginfo('template_url'); ?>/images/turntable.gif" alt="DJ"/>
</div><!--/content-->

		
<?php include ('sidebar2.php'); ?>

<div class="wide-wrapper artists-page corners clearfix">
<h2 class="title"><span>Artists</span></h2>
<div class="clearBoth"></div>

		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
				
					<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args=array(
					   'posts_per_page' => -1,
					   'post_type' => 'page',
					   'post_parent'=>4,
					   'paged'=>$paged,
					   'order'=>DESC,
					   );
					query_posts($args);
					?>
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="artist-box">
						
						<?php
						if ( has_post_thumbnail() ) {
							// the current post has a thumbnail
							the_post_thumbnail('artist-thumbnail');
						} else {
							// the current post lacks a thumbnail
						}
						?>
						
						<div class="artist-info">
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
						
						<?php $artist_link = get('artist_link');

						if($artist_link){?>
						<a href="<?php echo $artist_link ?>"><?php echo $artist_link ?></a>
						<?php }?>
						</div>
						
						<div class="artist-songs">
						<?php echo get('artist_featuredsongs'); ?>
						</div>
						
						<?php edit_post_link('Edit', '<p>', '</p>'); ?>
						
						
					</div>
						
					
					<?php endwhile; endif; ?>
				    
				    
				    <?php 
					wp_reset_query();
					?>
					
				
			<?php endwhile; ?>
		<?php else : ?>
		
			<div class="post">
				<h2><?php _e('Not Found'); ?></h2>
			</div>
			
		<?php endif; ?>
</div><!--/wrapper-->

<?php get_footer(); ?>

