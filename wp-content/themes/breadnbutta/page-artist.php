<?php
/*
Template Name: Artists Single Page
*/
?>

<?php get_header(); ?>

<div class="content-top">
	    <img src="<?php bloginfo('template_url'); ?>/images/turntable.gif" alt="DJ"/>
</div><!--/content-->

		
<?php include ('sidebar2.php'); ?>

<div class="wide-wrapper artist-page corners clearfix">
<h2 class="title"><span>Artists</span></h2>
	<div class="artist-info">
		<h3><?php the_title(); ?></h3>
		
		<?php $artist_link = get('artist_link');
		
		if($artist_link){?>
		<a href="<?php echo $artist_link ?>"><?php echo $artist_link ?></a>
		<?php }?>
	</div>
<div class="clearBoth"></div>

		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post(); ?>
							
				<?php
						if ( has_post_thumbnail() ) {
							// the current post has a thumbnail
							the_post_thumbnail('artist-big');
						} else {
							// the current post lacks a thumbnail
						}
				?>
				
				<div class="content-left">		
					<h3 class="title"><?php the_title(); ?></h3>
					<?php $artist_link = get('artist_link');

						if($artist_link){?>
						<a href="<?php echo $artist_link ?>"><?php echo $artist_link ?></a>
					<?php }?>
					
					<div class="entry">						
												
						<?php the_content(); ?>
				
					<?php edit_post_link('Edit', '<p>', '</p>'); ?>
					</div>
					
				</div><!--/content-left-->
				
					
				<div class="artist-songs">
				<h4><?php the_title(); ?> songs:</h4>
						<?php echo get('artist_songs'); ?>
				</div>
					
					
					
			</div><!--/post-->
					
				
			<?php endwhile; ?>
		<?php else : ?>
		
			<div class="post">
				<h2><?php _e('Not Found'); ?></h2>
			</div>
			
		<?php endif; ?>
</div><!--/wrapper-->

<div class="back-button">
	<a href="<?php bloginfo('url'); ?>/artists"><img src="<?php bloginfo('template_url'); ?>/images/back.gif" alt="Back"/></a>
</div>

<?php get_footer(); ?>

