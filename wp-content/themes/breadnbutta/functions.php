<?php


automatic_feed_links();

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true ); // Normal post thumbnails
	add_image_size( 'artist-thumbnail', 300, 155, true );
	add_image_size( 'artist-big', 940, 500, true ); 
	
}

function my_init() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js', false, '1.3.2', true);
		wp_enqueue_script('jquery');
	}
}
add_action('init', 'my_init');

function addSpace() {
    return '<br/>';
}

add_shortcode('space', 'addSpace');
    
?>