<div id="sidebar">    
    <div class="nav">
    	<ul>	    	
	    	<li class="nav-home"><a href="<?php bloginfo('url'); ?>"><span>home</span></a></li>
	    	<li class="nav-artists"><a href="<?php bloginfo('url'); ?>/artists"><span>artists</span></a></li>
	    	<li class="nav-brokebeats"><a href="<?php bloginfo('url'); ?>/brokebeats"><span>brokebeats</span></a></li>
	    	<li class="nav-services"><a href="<?php bloginfo('url'); ?>/services"><span>services</span></a></li>
	    	<li class="nav-links"><a href="<?php bloginfo('url'); ?>/links"><span>links</span></a></li>
	    	<li class="nav-contact"><a href="<?php bloginfo('url'); ?>/contact"><span>contact</span></a></li>
    	</ul>
    </div><!--/nav-->

</div><!--/sidebar-->